import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { CodescService, ContactPersonService } from '../../../services';
var SearchContactPersonComponent = /** @class */ (function () {
    function SearchContactPersonComponent(contactPersonService, codescService) {
        this.contactPersonService = contactPersonService;
        this.codescService = codescService;
        this.selectedValues = [];
    }
    SearchContactPersonComponent.prototype.ngOnInit = function () {
        var _this = this;
        //למלא מערך מסוג תוכניות programDto עם כל התוכניות
        //למלא מערך עם כל התפקידים CodescService
        this.codescService.GetSelectItemsByClassCode(120).subscribe(function (data) { return _this.dutyList = data; });
    };
    SearchContactPersonComponent = tslib_1.__decorate([
        Component({
            selector: 'search-contacts',
            templateUrl: '/SearchContactPerson.component.html',
        }),
        tslib_1.__metadata("design:paramtypes", [ContactPersonService, CodescService])
    ], SearchContactPersonComponent);
    return SearchContactPersonComponent;
}());
export { SearchContactPersonComponent };
//# sourceMappingURL=SearchContactPerson.component.js.map