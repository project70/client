import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { ContactPersonService } from '../../services';
var PersonContactComponent = /** @class */ (function () {
    function PersonContactComponent(personSrv) {
        this.personSrv = personSrv;
    }
    PersonContactComponent.prototype.ngOnInit = function () {
        // this.personSrv.GetAllContacts().subscribe(data => this.contactperson = data);
        this.items = [
            { label: 'Stats', icon: 'fa fa-fw fa-bar-chart' },
            { label: 'Calendar', icon: 'fa fa-fw fa-calendar' },
            { label: 'Documentation', icon: 'fa fa-fw fa-book' },
            { label: 'Support', icon: 'fa fa-fw fa-support' },
            { label: 'Social', icon: 'fa fa-fw fa-twitter' }
        ];
    };
    PersonContactComponent = tslib_1.__decorate([
        Component({
            selector: 'person',
            templateUrl: './contactPersonConponent.html',
        }),
        tslib_1.__metadata("design:paramtypes", [ContactPersonService])
    ], PersonContactComponent);
    return PersonContactComponent;
}());
export { PersonContactComponent };
//# sourceMappingURL=contactPersonConponent.js.map