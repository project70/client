import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { ContactPersonService } from 'src/app';
var ProductComponent = (function () {
    function ProductComponent(personSrv) {
        this.personSrv = personSrv;
    }
    ProductComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.personSrv.GetAllContacts().subscribe(function (data) { return _this.contactperson = data; });
    };
    return ProductComponent;
}());
ProductComponent = tslib_1.__decorate([
    Component({
        selector: 'product',
        templateUrl: './product.component.html',
    }),
    tslib_1.__metadata("design:paramtypes", [ContactPersonService])
], ProductComponent);
export { ProductComponent };
//# sourceMappingURL=contactPersonConponent.js.map