import { PersonContactComponent } from './contactPersonConponent';
import { PERSON_DETAILS_COMPONENTS } from './personGeneral';
export var CONTACT_COMPONENTS = [
    PersonContactComponent
].concat(PERSON_DETAILS_COMPONENTS);
export var CONTACT_ROUTES = [
    {
        path: 'contactperson/add',
        component: PersonContactComponent
    },
    {
        path: 'contactperson/:id',
        component: PersonContactComponent,
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'contactDetails'
            },
        ],
    },
];
//# sourceMappingURL=index.js.map