import { personGeneralComponent } from './personGeneral.component';
export * from './personGeneral.component';
export var PERSON_DETAILS_COMPONENTS = [
    personGeneralComponent
];
export var PERSON_DETAILS_ROUTES = [
    {
        path: 'personGeneral',
        component: personGeneralComponent
    },
];
//# sourceMappingURL=index.js.map