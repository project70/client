import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { ContactPersonService } from '../../..';
var personGeneralComponent = /** @class */ (function () {
    function personGeneralComponent(contactPersonsrv) {
        this.contactPersonsrv = contactPersonsrv;
    }
    personGeneralComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.contactP = new ContactPersonDto();
        this.loaded = false;
        this.contactPersonsrv.getContactPersonById(this.personId).subscribe(function (data) {
            _this.currentPerson = data;
            _this.loaded = true;
        });
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Number)
    ], personGeneralComponent.prototype, "personId", void 0);
    personGeneralComponent = tslib_1.__decorate([
        Component({
            selector: 'person-general',
            templateUrl: './personGeneral.component.html',
        }),
        tslib_1.__metadata("design:paramtypes", [ContactPersonService])
    ], personGeneralComponent);
    return personGeneralComponent;
}());
export { personGeneralComponent };
//# sourceMappingURL=personGeneral.component.js.map