import { ProductComponent } from './product.component';
export * from './product.component';
export var PRODUCT_COMPONENTS = [
    ProductComponent
];
export var PRODUCTS_ROUTES = [
    {
        path: 'Products',
        component: ProductComponent
    },
];
//# sourceMappingURL=index.js.map