import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { ProductService } from '../../services/';
var ProductComponent = /** @class */ (function () {
    function ProductComponent(productSrv) {
        this.productSrv = productSrv;
    }
    ProductComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.productSrv.getAllProducts().subscribe(function (data) { return _this.products = data; });
    };
    ProductComponent = tslib_1.__decorate([
        Component({
            selector: 'product',
            templateUrl: './product.component.html',
        }),
        tslib_1.__metadata("design:paramtypes", [ProductService])
    ], ProductComponent);
    return ProductComponent;
}());
export { ProductComponent };
//# sourceMappingURL=product.component.js.map