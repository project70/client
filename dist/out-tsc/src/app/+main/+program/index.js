import { ProgramComponent } from './program.component';
import { PROGRAM_DETAILS_COMPONENTS } from './programDetails';
import { PROGRAM_DETAILS_ROUTES } from './programDetails';
import { SEARCH_PROGRAM_ROUTES } from './search-program';
import { SEARCH_PROGRAM_COMPONENTS } from './search-program';
import { PROGRAM_CONTACTS_COMPONENTS, PROGRAM_CONTACTS_ROUTES } from './programContactPerson';
//'./ProgramAdvertisingComponent';
import { PROGRAM_PRODUCT_COMPONENTS, PROGRAM_PRODUCT_ROUTES } from './programProduct';
export var PROGRAMS_COMPONENTS = [
    ProgramComponent
].concat(PROGRAM_DETAILS_COMPONENTS, SEARCH_PROGRAM_COMPONENTS, PROGRAM_PRODUCT_COMPONENTS, PROGRAM_CONTACTS_COMPONENTS);
export var PROGRAMS_ROUTES = [
    {
        path: 'program/add',
        component: ProgramComponent
    },
    {
        path: 'program/:id',
        component: ProgramComponent,
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'programDetails'
            }
        ].concat(PROGRAM_DETAILS_ROUTES, PROGRAM_PRODUCT_ROUTES, PROGRAM_CONTACTS_ROUTES),
    }
].concat(SEARCH_PROGRAM_ROUTES);
//# sourceMappingURL=index.js.map