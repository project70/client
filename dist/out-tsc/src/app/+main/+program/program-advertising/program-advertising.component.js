import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { ProgramService } from '../../../services/Main/program.service';
//import { SelectItem } from 'primeng/api';
var ProgramAdvertisingComponent = /** @class */ (function () {
    function ProgramAdvertisingComponent(programService) {
        this.programService = programService;
    }
    ProgramAdvertisingComponent.prototype.alert1 = function () {
        alert('hila');
    };
    ProgramAdvertisingComponent.prototype.ngOnInit = function () {
    };
    ProgramAdvertisingComponent.prototype.searchAdvertisingByProgramId = function (event) {
        var _this = this;
        this.programService.getProgramAdvertising(this.programId).subscribe(function (data) { return _this.list = data; });
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Number)
    ], ProgramAdvertisingComponent.prototype, "programId", void 0);
    ProgramAdvertisingComponent = tslib_1.__decorate([
        Component({
            selector: 'app-program-advertising',
            templateUrl: './program-advertising.component.html',
            styleUrls: ['./program-advertising.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [ProgramService])
    ], ProgramAdvertisingComponent);
    return ProgramAdvertisingComponent;
}());
export { ProgramAdvertisingComponent };
//# sourceMappingURL=program-advertising.component.js.map