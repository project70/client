import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var ProgramComponent = /** @class */ (function () {
    function ProgramComponent() {
    }
    ProgramComponent.prototype.ngOnInit = function () {
        this.items = [
            { label: 'פרטים כלליים', icon: 'fa fa-fw fa-bar-chart' },
            { label: 'מוצרים', icon: 'fa fa-fw fa-calendar' },
            { label: 'אנשי קשר', icon: 'fa fa-fw fa-book' },
            { label: 'לוח ארועים', icon: 'fa fa-fw fa-support' },
            { label: 'פרסום והפצה', icon: 'fa fa-fw fa-twitter' }
        ];
    };
    ProgramComponent = tslib_1.__decorate([
        Component({
            selector: 'app-program',
            templateUrl: './program.component.html',
            styleUrls: ['./program.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], ProgramComponent);
    return ProgramComponent;
}());
export { ProgramComponent };
//# sourceMappingURL=program.component.js.map