import { ProgramContactPersonComponent } from './programContactPerson.component';
export * from './programContactPerson.component';
export var PROGRAM_CONTACTS_COMPONENTS = [
    ProgramContactPersonComponent
];
export var PROGRAM_CONTACTS_ROUTES = [
    {
        path: 'ProgramContactPerson',
        component: ProgramContactPersonComponent
    },
];
//# sourceMappingURL=index.js.map