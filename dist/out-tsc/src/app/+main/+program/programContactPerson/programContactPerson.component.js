import * as tslib_1 from "tslib";
import { ProgramService } from '../../../services';
import { Component, Input } from '@angular/core';
var ProgramContactPersonComponent = /** @class */ (function () {
    function ProgramContactPersonComponent(searchProgramsService) {
        this.searchProgramsService = searchProgramsService;
    }
    ProgramContactPersonComponent.prototype.ngOnInit = function () {
    };
    ProgramContactPersonComponent.prototype.searchProgramProducts = function (e) {
        var _this = this;
        this.searchProgramsService.getProgramContactPerson(this.programId)
            .subscribe(function (data) { return _this.list = data; });
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Number)
    ], ProgramContactPersonComponent.prototype, "programId", void 0);
    ProgramContactPersonComponent = tslib_1.__decorate([
        Component({
            selector: 'program-contact-person',
            templateUrl: '/programContactPerson.component.html',
        }),
        tslib_1.__metadata("design:paramtypes", [ProgramService])
    ], ProgramContactPersonComponent);
    return ProgramContactPersonComponent;
}());
export { ProgramContactPersonComponent };
//# sourceMappingURL=programContactPerson.component.js.map