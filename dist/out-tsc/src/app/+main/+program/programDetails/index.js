import { programDetails } from './programDetails.component';
export * from './programDetails.component';
export var PROGRAM_DETAILS_COMPONENTS = [
    programDetails
];
export var PROGRAM_DETAILS_ROUTES = [
    {
        path: 'programDetails',
        component: programDetails
    },
];
//# sourceMappingURL=index.js.map