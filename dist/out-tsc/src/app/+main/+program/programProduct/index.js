import { programProduct } from './programProduct';
export * from './programProduct';
export var PROGRAM_PRODUCT_COMPONENTS = [
    programProduct
];
export var PROGRAM_PRODUCT_ROUTES = [
    {
        path: 'programProduct',
        component: programProduct
    },
];
//# sourceMappingURL=index.js.map