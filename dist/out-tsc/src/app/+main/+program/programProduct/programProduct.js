import * as tslib_1 from "tslib";
import { ProgramService } from '../../../services';
import { Component, Input } from '@angular/core';
var programProduct = (function () {
    function programProduct(searchProgramsService) {
        this.searchProgramsService = searchProgramsService;
    }
    programProduct.prototype.ngOnInit = function () {
    };
    programProduct.prototype.searchProgramProducts = function (e) {
        var _this = this;
        this.searchProgramsService.GetAllProgramProductById(this.programId)
            .subscribe(function (data) { return _this.list = data; });
    };
    return programProduct;
}());
tslib_1.__decorate([
    Input(),
    tslib_1.__metadata("design:type", Number)
], programProduct.prototype, "programId", void 0);
programProduct = tslib_1.__decorate([
    Component({
        selector: 'program-product',
        templateUrl: '/programProduct.html',
    }),
    tslib_1.__metadata("design:paramtypes", [ProgramService])
], programProduct);
export { programProduct };
//# sourceMappingURL=programProduct.js.map