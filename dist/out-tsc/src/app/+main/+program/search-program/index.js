import { SearchProgramComponent } from './search-program.component';
export * from './search-program.component';
export var SEARCH_PROGRAM_COMPONENTS = [
    SearchProgramComponent
];
export var SEARCH_PROGRAM_ROUTES = [
    {
        path: 'SearchPrograms',
        component: SearchProgramComponent
    },
];
//# sourceMappingURL=index.js.map