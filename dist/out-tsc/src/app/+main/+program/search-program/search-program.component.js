import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { ProgramService } from '../../../services';
import { ContactPersonService } from '../../../services/Main/contactPerson.service';
var SearchProgramComponent = (function () {
    function SearchProgramComponent(searchProgramsService, contactPersonService) {
        this.searchProgramsService = searchProgramsService;
        this.contactPersonService = contactPersonService;
        this.wt = "s";
    }
    SearchProgramComponent.prototype.ngOnInit = function () {
        this.contactResults = [];
        this.GetContactPerson();
    };
    SearchProgramComponent.prototype.GetContactPerson = function () {
        var _this = this;
        this.contactPersonService.GetAllContacts()
            .subscribe(function (data) {
            _this.contactPersonList = _this.contactPersonService.convertContactSelectItemList(data);
            console.log(_this.contactPersonList);
        });
    };
    SearchProgramComponent.prototype.SearchContacts = function (event) {
        this.contactResults = this.contactPersonList.filter(function (c) { return c.label.toLowerCase().includes(event.query.toLowerCase()); });
    };
    return SearchProgramComponent;
}());
SearchProgramComponent = tslib_1.__decorate([
    Component({
        selector: 'search-program',
        templateUrl: './search-program.component.html',
        styleUrls: ['./search-program.component.css']
    }),
    tslib_1.__metadata("design:paramtypes", [ProgramService, ContactPersonService])
], SearchProgramComponent);
export { SearchProgramComponent };
//# sourceMappingURL=search-program.component.js.map