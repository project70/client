import { MainComponent } from './main.component';
import { DASHBOARD_ROUTES, DASHBOARD_COMPONENTS } from './+dashboard';
import { PROGRAMS_ROUTES, PROGRAMS_COMPONENTS } from './+program';
import { PRODUCT_COMPONENTS, PRODUCTS_ROUTES } from './+product';
import { CONTACT_COMPONENTS, CONTACT_ROUTES } from './+contactPerson';
//import { AuthGuard } from '../services/auto.guard'
export * from './+dashboard';
export var MAIN_COMPONENTS = [
    MainComponent
].concat(DASHBOARD_COMPONENTS, PROGRAMS_COMPONENTS, PRODUCT_COMPONENTS, CONTACT_COMPONENTS);
export var MAIN_ROUTES = [
    {
        path: 'main',
        component: MainComponent,
        //canActivate: [AuthGuard],
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'dashboard'
            }
        ].concat(DASHBOARD_ROUTES, PROGRAMS_ROUTES, PRODUCTS_ROUTES, CONTACT_ROUTES)
    }
];
//# sourceMappingURL=index.js.map