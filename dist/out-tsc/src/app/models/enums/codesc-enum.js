export var CodescEnum;
(function (CodescEnum) {
    CodescEnum[CodescEnum["ProgramStatus"] = 100] = "ProgramStatus";
    CodescEnum[CodescEnum["ProgramType"] = 101] = "ProgramType";
    CodescEnum[CodescEnum["Cycle"] = 102] = "Cycle";
    CodescEnum[CodescEnum["ExecutionLevel"] = 103] = "ExecutionLevel";
    CodescEnum[CodescEnum["Category"] = 104] = "Category";
    CodescEnum[CodescEnum["UseType"] = 105] = "UseType";
    CodescEnum[CodescEnum["MeasureUnit"] = 106] = "MeasureUnit";
    CodescEnum[CodescEnum["OrderType"] = 107] = "OrderType";
    CodescEnum[CodescEnum["Gender"] = 108] = "Gender";
    CodescEnum[CodescEnum["FamilyStatus"] = 109] = "FamilyStatus";
    CodescEnum[CodescEnum["Time"] = 110] = "Time";
    CodescEnum[CodescEnum["Day"] = 111] = "Day";
    CodescEnum[CodescEnum["Duty"] = 112] = "Duty";
    CodescEnum[CodescEnum["Creation"] = 113] = "Creation";
    CodescEnum[CodescEnum["ClassCode"] = 114] = "ClassCode";
})(CodescEnum || (CodescEnum = {}));
//# sourceMappingURL=codesc-enum.js.map