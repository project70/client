import * as tslib_1 from "tslib";
import { Injectable } from "@angular/core";
import 'rxjs/add/operator/map';
import { BaseApiService } from "../../shared";
import { BaseHttpService } from "../../shared/services/base-http.service";
import { CodescEnum } from "../../models/enums";
var CodescService = /** @class */ (function (_super) {
    tslib_1.__extends(CodescService, _super);
    function CodescService(baseHttpService) {
        var _this = _super.call(this, 'Codesc') || this;
        _this.baseHttpService = baseHttpService;
        return _this;
    }
    CodescService.prototype.GetCodescList = function (classCode) {
        var url = this.actionUrl('GetCodescListByClassCode');
        return this.baseHttpService.get(url, classCode);
    };
    CodescService.prototype.GetCodescListByClassCode = function (classCode) {
        var codescList = undefined;
        switch (classCode) {
            case CodescEnum.ProgramStatus:
                {
                    codescList = this.programStatusList;
                    break;
                }
            case CodescEnum.ProgramType:
                {
                    codescList = this.programTypeList;
                    break;
                }
            case CodescEnum.Cycle:
                {
                    codescList = this.cycleList;
                    break;
                }
            case CodescEnum.ExecutionLevel:
                {
                    codescList = this.executionLevelList;
                    break;
                }
            case CodescEnum.Category:
                {
                    codescList = this.categoryList;
                    break;
                }
            case CodescEnum.UseType:
                {
                    codescList = this.useTypeList;
                    break;
                }
            case CodescEnum.MeasureUnit:
                {
                    codescList = this.measureUnitList;
                    break;
                }
            case CodescEnum.OrderType:
                {
                    codescList = this.orderTypeList;
                    break;
                }
            case CodescEnum.Gender:
                {
                    codescList = this.genderList;
                    break;
                }
            case CodescEnum.FamilyStatus:
                {
                    codescList = this.familyStatusList;
                    break;
                }
            case CodescEnum.Time:
                {
                    codescList = this.timeList;
                    break;
                }
            case CodescEnum.Day:
                {
                    codescList = this.dayList;
                    break;
                }
            case CodescEnum.Duty:
                {
                    codescList = this.dutyList;
                    break;
                }
            case CodescEnum.Creation:
                {
                    codescList = this.creationList;
                    break;
                }
            case CodescEnum.ClassCode:
                {
                    codescList = this.classCodeList;
                    break;
                }
        }
        if (codescList != undefined) {
            return codescList;
        }
        else {
            codescList = this.GetCodescList(classCode);
            return codescList;
        }
    };
    CodescService.prototype.GetSelectItemsByClassCode = function (classCode) {
        this.GetCodescListByClassCode(classCode).subscribe(function (data) {
            var codescList = data;
            return codescList.map(function (item) { var a = { label: item.description, value: item.code }; return a; });
        }, function (error) { return console.log(error); });
        return null;
    };
    CodescService.prototype.GetDescriptionByCode = function (code) {
        if (code.toString.length < 7) {
            return "";
        }
        var classCode = Number(String(code).substring(0, 3));
        var codescClassCodeList;
        this.GetCodescListByClassCode(classCode).subscribe(function (data) {
            codescClassCodeList = data;
        }, function (error) { return console.log(error); });
        for (var _i = 0, codescClassCodeList_1 = codescClassCodeList; _i < codescClassCodeList_1.length; _i++) {
            var codescDTO = codescClassCodeList_1[_i];
            if (codescDTO.code == code)
                return codescDTO.description;
        }
        return "";
    };
    CodescService.prototype.convertCodescSelectItemList = function (list) {
        var codescList = list;
        return codescList.map(function (item) { var a = { label: item.description, value: item.code }; return a; });
    };
    CodescService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [BaseHttpService])
    ], CodescService);
    return CodescService;
}(BaseApiService));
export { CodescService };
//# sourceMappingURL=codesc.service.js.map