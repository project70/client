﻿import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
//import { Http } from '@angular/http';
import { BaseApiService, BaseHttpService } from "../../shared";
var ContactPersonService = (function (_super) {
    tslib_1.__extends(ContactPersonService, _super);
    function ContactPersonService(baseHttpService) {
        var _this = _super.call(this, 'ContactPerson') || this;
        _this.baseHttpService = baseHttpService;
        return _this;
    }
    ContactPersonService.prototype.GetAllContact = function (obj) {
        return null;
    };
    ContactPersonService.prototype.GetAllContacts = function () {
        var url = this.actionUrl('GetAllContactPerson');
        return this.baseHttpService.get(url);
    };
    ContactPersonService.prototype.convertContactSelectItemList = function (list) {
        return list.map(function (item) { var a = { label: item.firstName + ' ' + item.lastName, value: item.personId }; return a; });
    };
    ContactPersonService.prototype.GetAllContactsSelect = function () {
        var url = this.actionUrl('GetAllContactPerson');
        this.baseHttpService.get(url)
            .subscribe(function (data) {
            var codescList = data;
            var listItems = codescList.map(function (item) {
                var a = { label: item.firstName + ' ' + item.lastName,
                    value: item.personId };
                return a;
            });
            return listItems;
        }, function (error) { return console.log(error); });
        return null;
    };
    ContactPersonService.prototype.searchContactPerson = function (searchParams) {
        var url = this.actionUrl('SearchContactPerson');
        return this.baseHttpService.post(url, searchParams);
    };
    return ContactPersonService;
}(BaseApiService));
ContactPersonService = tslib_1.__decorate([
    Injectable(),
    tslib_1.__metadata("design:paramtypes", [BaseHttpService])
], ContactPersonService);
export { ContactPersonService };
//# sourceMappingURL=contactPerson.service.js.map