import { CodescService } from './codesc.service';
import { ProgramService } from './program.service';
import { ProductService } from './product.service';
import { ContactPersonService } from './contactPerson.service';
export * from './program.service';
export * from './codesc.service';
export * from './product.service';
export * from './contactPerson.service';
export var MAIN_SERVICES = [
    CodescService,
    ProgramService,
    ProductService,
    ContactPersonService
];
//# sourceMappingURL=index.js.map