import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BaseApiService, BaseHttpService } from "../../shared";
var ProductService = /** @class */ (function (_super) {
    tslib_1.__extends(ProductService, _super);
    function ProductService(baseHttpService) {
        var _this = _super.call(this, 'ProductInventory') || this;
        _this.baseHttpService = baseHttpService;
        return _this;
    }
    ProductService.prototype.getAllProducts = function () {
        var url = this.actionUrl('GetProducts');
        return this.baseHttpService.get(url);
    };
    ProductService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [BaseHttpService])
    ], ProductService);
    return ProductService;
}(BaseApiService));
export { ProductService };
//# sourceMappingURL=product.service.js.map