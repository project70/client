import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BaseApiService, BaseHttpService } from "../../shared";
var ProgramService = /** @class */ (function (_super) {
    tslib_1.__extends(ProgramService, _super);
    function ProgramService(baseHttpService) {
        var _this = _super.call(this, 'Program') || this;
        _this.baseHttpService = baseHttpService;
        return _this;
    }
    // public GetAllContact() :Observable <ContactPersonDto[]>{
    //     let url = this.actionUrl('GetAllContactPerson');
    //     return this.baseHttpService.get<ContactPersonDto[]>(url);
    // }
    // public GetAllContacts() : Observable <ContactPersonDto[]> {
    //     this.GetAllContact().subscribe((data: ContactPersonDto[])=>
    //     {
    //         let codescList=data;
    //         return codescList.map(item => {var a: SelectItem = { label:item.FirstName + ' ' + item.LastName, value:item.PersonId };return a;});
    //     },
    //   (error)=>console.log(error));
    //     return null;   
    //  }
    ProgramService.prototype.GetAllProgramProduct = function () {
        var url = this.actionUrl('GetAllProgramProduct');
        return this.baseHttpService.get(url);
    };
    ProgramService.prototype.GetAllProgramProductById = function (programId) {
        var params = new URLSearchParams();
        var url = this.actionUrl('GetProductsByProgramId');
        params.set('programId', programId);
        return this.baseHttpService.get(url, params);
    };
    ProgramService.prototype.getProgramAdvertising = function (id) {
        var params = new URLSearchParams();
        var url = this.actionUrl('GetAdvertisingByProgramId');
        params.set('programId', id);
        return this.baseHttpService.get(url, params);
    };
    //מקבלת אנשי קשר לתכנית 
    //ותשלח לWebApi
    ProgramService.prototype.getProgramContactPerson = function (programId) {
        var params = new URLSearchParams();
        var url = this.actionUrl('GetContactsByProgramId');
        params.set('programId', programId);
        return this.baseHttpService.get(url, params);
    };
    ProgramService.prototype.searchPrograms = function (searchParams) {
        var url = this.actionUrl('SearchPrograms');
        return this.baseHttpService.get(url, searchParams);
    };
    ProgramService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [BaseHttpService])
    ], ProgramService);
    return ProgramService;
}(BaseApiService));
export { ProgramService };
//# sourceMappingURL=program.service.js.map