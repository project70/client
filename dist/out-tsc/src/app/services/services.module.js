import * as tslib_1 from "tslib";
import { NgModule } from "@angular/core";
import { AUTH_SERVICES } from "./Auth";
import { MAIN_SERVICES } from "./Main";
var SERVICES = AUTH_SERVICES.concat(MAIN_SERVICES);
var ServicesModule = /** @class */ (function () {
    function ServicesModule() {
    }
    ServicesModule = tslib_1.__decorate([
        NgModule({
            providers: SERVICES.slice()
        })
    ], ServicesModule);
    return ServicesModule;
}());
export { ServicesModule };
//# sourceMappingURL=services.module.js.map