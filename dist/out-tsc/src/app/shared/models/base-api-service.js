import { environment } from "../../../environments/environment";
import { HttpHeaders } from "@angular/common/http";
var BASE_HTTP_METHODS = ['get', 'post', 'put', 'delete'];
var BaseApiService = /** @class */ (function () {
    function BaseApiService() {
        var serviceRouteParts = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            serviceRouteParts[_i] = arguments[_i];
        }
        this.apiServer = environment.apiServer;
        //portNumber: number = this.apiServer.port;
        this.useHttps = this.apiServer.useHttps;
        this.serverUrl = this.apiServer.serverUrl;
        this.fullAddress = this.apiServer.fullAddress();
        this._serviceRouteParts = serviceRouteParts;
    }
    Object.defineProperty(BaseApiService.prototype, "basePath", {
        get: function () {
            return this.fullAddress + "api";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseApiService.prototype, "requestOptions", {
        get: function () {
            var headers = new HttpHeaders();
            headers = headers.set('Content-Type', 'application/json')
                .set('Accept', 'application/json');
            var options = {
                headers: headers
            };
            return options;
        },
        enumerable: true,
        configurable: true
    });
    BaseApiService.prototype.actionUrl = function (actionName, params) {
        if (BASE_HTTP_METHODS.indexOf(actionName.toLowerCase()) > -1) //action url 
            actionName = '';
        var middle = this._serviceRouteParts.join('/');
        var more = params && params.length > 0 ? '/' + params.join('/') : '';
        return this.basePath + "/" + middle + "/" + actionName + more;
    };
    return BaseApiService;
}());
export { BaseApiService };
//# sourceMappingURL=base-api-service.js.map