import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
var dOrderCodePipe = (function () {
    function dOrderCodePipe() {
    }
    dOrderCodePipe.prototype.transform = function (value) {
        return GetSelectItemsByClassCode();
    };
    return dOrderCodePipe;
}());
dOrderCodePipe = tslib_1.__decorate([
    Pipe({ name: 'dOrderCode' })
], dOrderCodePipe);
export { dOrderCodePipe };
//# sourceMappingURL=tableCodePipe.js.map