import * as tslib_1 from "tslib";
import { Injectable } from "@angular/core";
import 'rxjs/add/operator/map';
import { BaseApiService, BaseHttpService } from "../../shared";
import { personTypeEnum } from "../../models";
var AutoCompleteService = /** @class */ (function (_super) {
    tslib_1.__extends(AutoCompleteService, _super);
    function AutoCompleteService(baseHttpService) {
        var _this = _super.call(this, 'Persons') || this;
        _this.baseHttpService = baseHttpService;
        return _this;
    }
    AutoCompleteService.prototype.getPeople = function (lettersContained, personType, contains) {
        var url;
        var params = new URLSearchParams();
        if (contains) {
            switch (+personType) {
                case personTypeEnum.femaleCandidate:
                    {
                        url = this.actionUrl('GetNamesOfCandidateByGenderContain');
                        params.set('genderCode', personType);
                        break;
                    }
                case personTypeEnum.maleCandidate:
                    {
                        url = this.actionUrl('GetNamesOfCandidateByGenderContain');
                        params.set('genderCode', personType);
                        break;
                    }
                default:
                    {
                        url = this.actionUrl('GetAllNamePersons');
                        break;
                    }
            }
            params.set('contain', lettersContained);
        }
        else {
            switch (+personType) {
                case personTypeEnum.femaleCandidate:
                    {
                        url = this.actionUrl('GetNamesOfCandidateByGenderStartWith');
                        params.set('genderCode', personType);
                        break;
                    }
                case personTypeEnum.maleCandidate:
                    {
                        url = this.actionUrl('GetNamesOfCandidateByGenderStartWith');
                        params.set('genderCode', personType);
                        break;
                    }
                default:
                    {
                        url = this.actionUrl('GetAllNamePersons');
                        break;
                    }
            }
            params.set('startWith', lettersContained);
        }
        return this.baseHttpService.get(url, params);
    };
    AutoCompleteService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [BaseHttpService])
    ], AutoCompleteService);
    return AutoCompleteService;
}(BaseApiService));
export { AutoCompleteService };
//# sourceMappingURL=auto-complete.servise.js.map