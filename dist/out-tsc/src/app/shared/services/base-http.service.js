import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
var BaseHttpService = /** @class */ (function () {
    function BaseHttpService(http /*, private loginService:LoginService*/) {
        this.http = http;
    }
    BaseHttpService.prototype.get = function (url, param) {
        var _this = this;
        return this.http.get(this.composeUrl(url, param), this.requestOptions)
            .map(function (res) { return _this.extractData(res); });
    };
    BaseHttpService.prototype.post = function (url, body) {
        var _this = this;
        return this.http.post(url, body, this.requestOptions)
            .map(function (res) { return _this.extractData(res); })
            .catch(function (err) {
            alert(err);
            // simple logging, but you can do a lot more, see below
            console.error('An error occurred:', err.error);
            return _this.handleError(err);
        });
        //.catch((err) => this.handleError<T>(err));
    };
    BaseHttpService.prototype.put = function (url, body) {
        var _this = this;
        return this.http.put(url, body, this.requestOptions)
            .map(function (res) { return _this.extractData(res); })
            .catch(function (err) { return _this.handleError(err); });
    };
    BaseHttpService.prototype.delete = function (url, param) {
        var _this = this;
        return this.http.delete(this.composeUrl(url, param), this.requestOptions)
            .map(function (res) { return _this.extractData(res); })
            .catch(function (err) { return _this.handleError(err); });
    };
    BaseHttpService.prototype.composeUrl = function (url, param) {
        if (url.lastIndexOf('/') == url.length - 1)
            url = url.substr(0, url.length - 2);
        if (param) {
            if (typeof (param) === 'object') { // URLSearchParams
                url += '?' + param;
            }
            else {
                url += '/' + param;
            }
        }
        return url;
    };
    BaseHttpService.prototype.handleError = function (error) {
        console.log(error);
        return;
    };
    BaseHttpService.prototype.extractData = function (response) {
        return response.body;
    };
    Object.defineProperty(BaseHttpService.prototype, "requestOptions", {
        get: function () {
            var headers = new HttpHeaders();
            headers = headers.set('Content-Type', 'application/json')
                .set('Accept', 'application/json');
            var options = {
                observe: 'response',
                headers: headers
            };
            return options;
        },
        enumerable: true,
        configurable: true
    });
    BaseHttpService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [HttpClient /*, private loginService:LoginService*/])
    ], BaseHttpService);
    return BaseHttpService;
}());
export { BaseHttpService };
//# sourceMappingURL=base-http.service.js.map