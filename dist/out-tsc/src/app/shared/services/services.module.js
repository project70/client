import * as tslib_1 from "tslib";
import { NgModule } from "@angular/core";
import { BaseHttpService } from './base-http.service';
import { CodescService } from './codesc.service';
export var SHARED_SERVICES = [
    BaseHttpService,
    CodescService
];
var SharedServicesModule = /** @class */ (function () {
    function SharedServicesModule() {
    }
    SharedServicesModule = tslib_1.__decorate([
        NgModule({
            providers: SHARED_SERVICES.slice()
        })
    ], SharedServicesModule);
    return SharedServicesModule;
}());
export { SharedServicesModule };
//# sourceMappingURL=services.module.js.map