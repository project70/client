import * as tslib_1 from "tslib";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { HttpClientModule } from "@angular/common/http";
import { SharedServicesModule } from "./services";
import { UixModule } from "./uix";
var EXPORTED_MODULES = [
    SharedServicesModule,
    UixModule
];
var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = tslib_1.__decorate([
        NgModule({
            imports: [
                BrowserModule,
                HttpClientModule,
            ],
            exports: EXPORTED_MODULES.slice(),
        })
    ], SharedModule);
    return SharedModule;
}());
export { SharedModule };
//# sourceMappingURL=shared.module.js.map