import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter } from "@angular/core";
import { AutoCompleteService } from "../../../services";
var AutoCompleteComponent = /** @class */ (function () {
    function AutoCompleteComponent(autoCompleteService) {
        this.autoCompleteService = autoCompleteService;
        this.value = new EventEmitter();
        this.contains = false;
        this.placeHolder = "";
        this.results = [];
    }
    ;
    AutoCompleteComponent.prototype.search = function (event) {
        var _this = this;
        var query = event.query;
        this.autoCompleteService.getPeople(query, this.personType, this.contains)
            .subscribe(function (data) {
            _this.results = data;
        });
    };
    AutoCompleteComponent.prototype.selected = function () {
        this.value.emit(this.val);
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Number)
    ], AutoCompleteComponent.prototype, "personType", void 0);
    tslib_1.__decorate([
        Output(),
        tslib_1.__metadata("design:type", Object)
    ], AutoCompleteComponent.prototype, "value", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Boolean)
    ], AutoCompleteComponent.prototype, "contains", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], AutoCompleteComponent.prototype, "placeHolder", void 0);
    AutoCompleteComponent = tslib_1.__decorate([
        Component({
            templateUrl: "./auto-complete.component.html",
            selector: "m-auto-complete"
        }),
        tslib_1.__metadata("design:paramtypes", [AutoCompleteService])
    ], AutoCompleteComponent);
    return AutoCompleteComponent;
}());
export { AutoCompleteComponent };
//# sourceMappingURL=auto-complete.component.js.map