import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private active:Router){

  }
  ngOnInit() {
    this.active.navigate(['/main'])
  }
  title = 'SmartParking';
  value: Date;
  isClick1:boolean= false;
  isClick2:boolean= false;
click(type){
  
  if  (type===1){
    this.isClick1=true
    this.isClick2=false
  }


  if  (type===2){
    this.isClick1=false
    this.isClick2=true
  }
}
//  constructor(private route:Router) { }
 
}

