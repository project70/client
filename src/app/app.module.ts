import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';


import { HttpClientModule } from '@angular/common/http';
import { MAT_DIALOG_DEFAULT_OPTIONS} from '@angular/material/dialog';
import {MatDialogModule} from '@angular/material/dialog';


import {MatDialogConfig} from '@angular/material/dialog';

import {AccordionModule} from 'primeng/accordion';     //accordion and accordion tab
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';    
import {CalendarModule} from 'primeng/calendar';
import { FormsModule } from '@angular/forms';
import { NainComponent } from './nain/nain.component';
import { RouterModule } from '@angular/router';
import { SearchComponent } from './search/search.component';
import {ListboxModule} from 'primeng/listbox';
// import {AccordionModule} from 'primeng/accordion';     //accordion and accordion tab
import {MenuItem} from 'primeng/api'; 
import {InputTextModule} from 'primeng/inputtext';
import {MultiSelectModule} from 'primeng/multiselect';
import {DropdownModule} from 'primeng/dropdown';
import { ProductDitailesComponent } from './product-ditailes/product-ditailes.component';
import { UseroldComponent } from './userold/userold.component';
import { VisaDitailesComponent } from './visa-ditailes/visa-ditailes.component';
import { NewuserComponent } from './newuser/newuser.component';
import { MechrazService } from './servies/mechraz.service';
import { NewauctuionComponent } from './newauctuion/newauctuion.component';
import { CategoriesService } from './servies/categories.service';
import {CardModule} from 'primeng/card';
import { BidsComponent } from './bids/bids.component';
import { UploadAuctuoinsComponent } from './upload-auctuoins/upload-auctuoins.component';
import { MyAuctionsComponent } from './my-auctions/my-auctions.component';
import { MyBidsComponent } from './my-bids/my-bids.component';
import { SuccessUploadPopupComponent } from './newauctuion/success-upload-popup/success-upload-popup.component';




//angular material
import {A11yModule} from '@angular/cdk/a11y';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {PortalModule} from '@angular/cdk/portal';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatBadgeModule} from '@angular/material/badge';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatButtonModule} from '@angular/material/button';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatChipsModule} from '@angular/material/chips';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatDividerModule} from '@angular/material/divider';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSliderModule} from '@angular/material/slider';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatTabsModule} from '@angular/material/tabs';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatTreeModule} from '@angular/material/tree';
@NgModule({
  declarations: [
  
  
    AppComponent,
  
  
    NainComponent,
    SearchComponent,
     ProductDitailesComponent,
      UseroldComponent,
      VisaDitailesComponent,
       NewuserComponent,
        NewauctuionComponent,
         BidsComponent,
          UploadAuctuoinsComponent,
    MyAuctionsComponent,
    MyBidsComponent,
    SuccessUploadPopupComponent



 
  ],
  imports: [
    HttpClientModule,
    ListboxModule,
    FormsModule,
    CalendarModule,
    AccordionModule,
    BrowserModule,
    BrowserAnimationsModule,
    MultiSelectModule,
    DropdownModule,
    InputTextModule,
    CardModule,
    MatDialogModule,


    //angular material
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    BrowserAnimationsModule,

    RouterModule.forRoot([
      {path:'search',component:SearchComponent},
      {path:'main',component:NainComponent},
      {path:'productditailes/:id',component:ProductDitailesComponent},
      {path:'newuser',component:NewuserComponent},
      {path:'visaditailes',component:VisaDitailesComponent},
      {path:'userold',component:UseroldComponent},
      {path:'Newauctuion',component:NewauctuionComponent},
      {path:'Bids/:id' , component:BidsComponent},
      {path:'UploadAuctuoin' , component:UploadAuctuoinsComponent},
      {path:'MyAuctuoins' , component:MyAuctionsComponent},
      {path:'MyBids' , component:MyBidsComponent}
    ])
 
  ],
  entryComponents:[
SuccessUploadPopupComponent
  ],
  providers: [MechrazService,CategoriesService,
    // {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
