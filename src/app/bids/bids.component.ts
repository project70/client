import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MechrazService } from '../servies/mechraz.service';
import { auctuion } from '../classes/auctuion';
import { offerTo_declared } from '../classes/offerTo_declared';

@Component({
  selector: 'app-bids',
  templateUrl: './bids.component.html',
  styleUrls: ['./bids.component.css']
})
export class BidsComponent implements OnInit {
  private auctuoin:auctuion;
  private open:boolean = false;
bids:Array<offerTo_declared> = new Array<offerTo_declared>();
  constructor(private active:ActivatedRoute,
    private michrazSerice:MechrazService) {
    this.active.params.subscribe(
      id=> {

        //select the auctouin from the service.
        this.auctuoin = this.michrazSerice.listauctuion.find(a=> a.id_auction == id["id"]);
        //if its after the close date. show the list.
if(this.auctuoin.date_close < new Date())
{
this.open = true;
}
      }
    )
    
   }

  ngOnInit() {
  }

}