export class sysdiagrams
{
    constructor(
    public  name :string,
    public  principal_id :number,
    public  diagram_id :number,
    public  version :number,
    public  definition :string,
    ){}

}