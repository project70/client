import { Component, OnInit } from '@angular/core';
import { auctuion } from '../classes/auctuion';
import { MechrazService } from '../servies/mechraz.service';
import { UserService } from '../servies/user.service';

@Component({
  selector: 'app-my-auctions',
  templateUrl: './my-auctions.component.html',
  styleUrls: ['./my-auctions.component.css']
})
export class MyAuctionsComponent implements OnInit {

  auctions:Array<auctuion> = new Array<auctuion>();
  constructor(private mechrazService:MechrazService,
    private userService:UserService) { 
    if ( this.mechrazService.listauctuion == null)
    {
      this.mechrazService.getCurrentUsersAuctuions(this.userService.CurrentUser.id).subscribe(
        data => {
          this.mechrazService.CurrentUsersAuctuions = data;

          this.mechrazService.getBidsToAuctuions(data).subscribe(
            offersData =>  this.mechrazService.ListOffers = offersData
          )
        }
      )
    }
    
    // this.auctions.push(new auctuion(1,  1 ,1, 'sfsdfs'  ,'מאזדה 3.jpg' , new Date() , ' ', new Date));
  }

  ngOnInit() {
  }

}

