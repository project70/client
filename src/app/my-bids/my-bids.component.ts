import { Component, OnInit } from '@angular/core';
import { MechrazService } from '../servies/mechraz.service';
import { UserService } from '../servies/user.service';

@Component({
  selector: 'app-my-bids',
  templateUrl: './my-bids.component.html',
  styleUrls: ['./my-bids.component.css']
})
export class MyBidsComponent implements OnInit {
  constructor(private auctiuoinService:MechrazService,
    private userService:UserService) { 

      
    this.auctiuoinService.getMyBids(this.userService.CurrentUser.id).subscribe(data=>{
      this.auctiuoinService.CurrentUsersBids = data;
    });

  }

  ngOnInit() {
  }

}