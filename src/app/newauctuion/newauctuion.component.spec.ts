import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewauctuionComponent } from './newauctuion.component';

describe('NewauctuionComponent', () => {
  let component: NewauctuionComponent;
  let fixture: ComponentFixture<NewauctuionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewauctuionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewauctuionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
