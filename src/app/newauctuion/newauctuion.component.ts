import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UserService } from '../servies/user.service';
import { people } from '../classes/people';
import { SuccessUploadPopupComponent } from './success-upload-popup/success-upload-popup.component';
import { auctuion } from '../classes/auctuion';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { MechrazService } from '../servies/mechraz.service';
import { CategoriesService } from '../servies/categories.service';

@Component({
  selector: 'app-newauctuion',
  templateUrl: './newauctuion.component.html',
  styleUrls: ['./newauctuion.component.css']
})
export class NewauctuionComponent implements OnInit {
  @ViewChild('fileUpload', { static: true }) fileUp: ElementRef<HTMLElement>;
  imgFileName: string = '';
  auctuion:auctuion = new auctuion(0 , this.userService.CurrentUser.id , 0 , '' , '' ,new Date( ) , '100' , new Date());
  constructor(private userService:UserService, 
    public dialog:MatDialog,
    private router:Router,
    private mecrazService:MechrazService,
    private categoriesService:CategoriesService) {

      this.categoriesService.gatAllCategories().subscribe(
        data=>{
          this.categoriesService.Categories = data;
        }
      );
     }
  currentUser: people;
  ngOnInit() {
    this.currentUser = this.userService.CurrentUser;
  }

  upload(){
    let el: HTMLElement = this.fileUp.nativeElement;
    el.click();
  }

  fileChange(event) {
    let fileList: FileList = event.target.files;
    if(fileList.length > 0) {
        let file: File = fileList[0];
        let formData:FormData = new FormData();
        formData.append('uploadFile', file, file.name);
        this.imgFileName = file.name;
        // let headers = new Headers();
        // /** In Angular 5, including the header Content-Type can invalidate your request */
        // headers.append('Content-Type', 'multipart/form-data');
        // headers.append('Accept', 'application/json');
        // let options = new RequestOptions({ headers: headers });
        // this.http.post(`${this.apiEndPoint}`, formData, options)
        //     .map(res => res.json())
        //     .catch(error => Observable.throw(error))
        //     .subscribe(
        //         data => console.log('success'),
        //         error => console.log(error)
        //     )
    }
}

//function that save the new auctuoin.
saveAndUpload()
{
  debugger;
  this.mecrazService.saveAuctuion(this.auctuion).subscribe(
    data=> {
      debugger;
      this.mecrazService.listauctuion = data;
      this.openDialog();
    }
  );
}

//function that open the dialog- popup.
openDialog(): void {
  let dialogRef = this.dialog.open(SuccessUploadPopupComponent,{
    panelClass:'my-dialog',
    width: '40vw',
    
    data: true
  });
  dialogRef.afterClosed().subscribe(result => {
    console.log('The dialog was closed');
    debugger;
    this.router.navigate(['/UploadAuctuoin']);
  });
}
}
