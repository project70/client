import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessUploadPopupComponent } from './success-upload-popup.component';

describe('SuccessUploadPopupComponent', () => {
  let component: SuccessUploadPopupComponent;
  let fixture: ComponentFixture<SuccessUploadPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuccessUploadPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccessUploadPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
