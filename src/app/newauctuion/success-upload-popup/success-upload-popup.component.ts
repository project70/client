import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-success-upload-popup',
  templateUrl: './success-upload-popup.component.html',
  styleUrls: ['./success-upload-popup.component.css']
})
export class SuccessUploadPopupComponent implements OnInit {
  constructor( public dialogRef: MatDialogRef<SuccessUploadPopupComponent>,
    //:DialogData
    public router:Router) { }

  ngOnInit() {
  }


  
  closePopup() {
    this.router.navigate(["/UploadAuctuoin"]);
  }

  
  onNoClick()
  {
    try{
          this.dialogRef.close();
    }
    catch
    {
      this.closePopup();
    }
  }
}
