import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductDitailesComponent } from './product-ditailes.component';

describe('ProductDitailesComponent', () => {
  let component: ProductDitailesComponent;
  let fixture: ComponentFixture<ProductDitailesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductDitailesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductDitailesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
