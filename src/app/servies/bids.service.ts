import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BidsService {
//now inited with fake data.
private bids:Array<string> = new Array<string>();
  constructor(private httpClient:HttpClient) { }
  get Bids()
  {
    return this.bids;
  }

  set Bids(value:Array<string>)
  {
    this.bids = value;
  }
}