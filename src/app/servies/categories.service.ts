import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { category } from '../classes/category';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {
  url:string="http://localhost:50103/api/Categories/"
  constructor(private http:HttpClient) { }
  
  //list of the categories.
  private categories:Array<category> = new Array<category>();
  //שליפת כל הקטגוריות
   gatAllCategories():Observable<Array<category>>
   {
     return this.http.get<Array<category>>(this.url+"getAll")
     
   }

   
   get Categories():Array<category>
   {
     return this.categories;
   }

   set Categories(value:Array<category>)
   {
     this.categories = value;
   }
}
