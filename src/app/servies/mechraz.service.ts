import { Injectable } from '@angular/core';
import { auctuion } from '../classes/auctuion';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SelectItem } from "primeng/api";
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { group } from "@angular/animations";
import { people } from '../classes/people';
import { offerTo_declared } from '../classes/offerTo_declared';

@Injectable({
  providedIn: 'root'
})
export class MechrazService {
 
  url:string="http://localhost:50103/api/auctuion/"
  listauctuion:Array<auctuion>=new Array<auctuion>();
  //offers to current users' auctuions.
  private listOffers:Array<offerTo_declared> = new Array<offerTo_declared>();
  private currentUsersBids:Array<offerTo_declared> =  new Array<offerTo_declared>();
  private currentUsersAuctuions:Array<auctuion> = new Array<auctuion>();

  constructor(private http:HttpClient) {
    
   }
   ///שליפת כל המכרזים
   getAllMechrazim():Observable<auctuion[]>
   {
     debugger;
     return this.http.get<Array<auctuion>>(this.url+"getAllMechrazim")
   }
   serch(araid:Array<number>,from?:number,to?:number):Observable<Array<auctuion>>
   {
     debugger;
    let fromStr, toStr;
     if(!from) fromStr = '0'; else fromStr = from.toString();
     if(!to) toStr = '0'; else toStr = to.toString();
     return this.http.post<Array<auctuion>>(this.url+"search/"+fromStr+"/"+toStr,araid)
   }
   login(mail: string, password: string): any {
     debugger;
     return this.http.get<any>(this.url+"login/?mail="+mail+"&password="+password)
   
   }
   addUser(user:people): any {
     return this.http.post<any>(this.url+"addUser",user)
   
   }

saveAuctuion(auctuion:auctuion):Observable<auctuion[]>
{
  return this.http.post<auctuion[]>(this.url+"addAuctuion" , auctuion );
}

//function that return the auctuions that the user want to buy.
getMyBids(userId:number):Observable<offerTo_declared[]>
{
  return this.http.get<offerTo_declared[]>(this.url + "getMyBids/" +userId);

}

get CurrentUsersBids():Array<offerTo_declared>
{
  return this.currentUsersBids;
}

set CurrentUsersBids(value:Array<offerTo_declared>)
{
  this.currentUsersBids = value;
}

//return all the offers to all the auctuion of the current user.
getBidsToAuctuions(auctuions:Array<auctuion>):Observable<offerTo_declared[]>
{
  return this.http.get<offerTo_declared[]>(this.url + "getBidsToAucuions/" + auctuions );
}

get ListOffers():Array<offerTo_declared>
{
  return this.listOffers;
}

set ListOffers(value:Array<offerTo_declared>)
{
  this.listOffers = value;
}

get CurrentUsersAuctuions():Array<auctuion>
{
  return this.currentUsersAuctuions;
}

set CurrentUsersAuctuions(value:Array<auctuion>)
{
  this.currentUsersAuctuions = value;
}

//return the auctuions of the current user.
getCurrentUsersAuctuions(id:number):Observable<auctuion[]>
{
  return this.http.get<auctuion[]>(this.url + "getCurrentUsersAuctuions" + id);
}

addOferrToDeclar(o:offerTo_declared){

  return this.http.post<any>("http://localhost:50103/api/offerTo_declare/"+"offerTo_declared",o)

}


}
