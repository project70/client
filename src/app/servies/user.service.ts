import { Injectable } from '@angular/core';
import { people } from '../classes/people';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private currentUser: people = {first_name: 'שרה', id:2, last_name:'בנימין', gmail:'', password:'SBen1234'};

  private page:number =  0;

  constructor(private httpClient:HttpClient)
  {

  }
  get CurrentUser():people{
      return this.currentUser;
  }

  set CurrentUser(value:people)
  {
    this.currentUser = value;
  }

  get Page():number
  {
    return this.page;
  }

  set Page(value:number)
  {
    this.page = value;
  }

}
