import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadAuctuoinsComponent } from './upload-auctuoins.component';

describe('UploadAuctuoinsComponent', () => {
  let component: UploadAuctuoinsComponent;
  let fixture: ComponentFixture<UploadAuctuoinsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadAuctuoinsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadAuctuoinsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
