import { Component, OnInit } from '@angular/core';
import { people } from '../classes/people';
import { UserService } from '../servies/user.service';

@Component({
  selector: 'app-upload-auctuoins',
  templateUrl: './upload-auctuoins.component.html',
  styleUrls: ['./upload-auctuoins.component.css']
})
export class UploadAuctuoinsComponent implements OnInit {

  user:people;
  constructor(private userService:UserService) { 
    this.user = this.userService.CurrentUser;
  }

  ngOnInit() {
  }

}
