import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UseroldComponent } from './userold.component';

describe('UseroldComponent', () => {
  let component: UseroldComponent;
  let fixture: ComponentFixture<UseroldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UseroldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UseroldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
