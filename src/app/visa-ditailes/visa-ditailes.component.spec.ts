import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisaDitailesComponent } from './visa-ditailes.component';

describe('VisaDitailesComponent', () => {
  let component: VisaDitailesComponent;
  let fixture: ComponentFixture<VisaDitailesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisaDitailesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisaDitailesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
